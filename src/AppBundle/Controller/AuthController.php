<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
   // public function register(Request $request, UserPasswordEncoderInterface $encoder)
    public function register(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $username = $request->query->get('_username');
        $password = $request->query->get('_password');
        
        $user = new User($username);
       // $user->setPassword($encoder->encodePassword($user, $password));
	    $encoder = $this->get('security.password_encoder');
        $password = $encoder->encodePassword($user, $password);
	    $user->setPassword( $password);
        $em->persist($user);
        $em->flush();

        return new Response(sprintf('User %s successfully created', $user->getUsername()));
    }

    public function api()
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }
}