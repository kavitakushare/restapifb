<?php
//---------
namespace AppBundle\Controller\Api\v1;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\League;
use AppBundle\Entity\Teams;
use Symfony\Component\HttpFoundation\Response;

class FootballController extends Controller
{

     /**
     * @Route("/api/v1/football")
     * @Method("POST")
     */
    public function newAction(Request $request) {


        $name = $request->query->get('name');

		// Create a new empty object
		$league = new League();

		// Use methods from the league entity to set the values
	
		$league->setName($name);
		// Get the Doctrine service and manager
		$em = $this->getDoctrine()->getManager();

		// Add our league to Doctrine so that it can be saved
		$em->persist($league);

		// Save our league
		$em->flush();

		return new Response('It\'s probably been saved', 201);


    }


	/**
	 * @Route("/api/v1/football/{id}")
	 * @Method("GET")
	 * @param $id
	 */
	public function getAction($id) {
		$teams = $this->getDoctrine()
        ->getRepository(Teams::class)
        ->findByLeague($id);
	if (!$teams) {
        throw $this->createNotFoundException(
            'No team found for league '.$id
        );
    }
	foreach($teams as $t)
	{
	$data[] = [
    'id' => $t->getId(),
    'name' => $t->getName(),
  ];
	}
	
	 
	return new Response(json_encode(['data'=>$data]));

	}


	/**
     * Deletes a League entity.
     *
     * @Route("api/v1/football/{id}")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
			$league = $this->getDoctrine()
			->getRepository(League::class)
			->find($id);
		if ($league === NULL) {
			return new Response('It\'s not found', 404);
		}
    
        $em =  $this->getDoctrine()->getManager();
		try {
			$em->remove($league);
			$em->flush();
		} catch (\Doctrine\DBAL\DBALException $e) {
			return new Response('This league has teams, so it can\'t delete ', 201);
		}
        return new Response('It\'s deleted', 201);
    }


}
?>