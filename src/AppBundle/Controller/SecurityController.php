<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;



class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
		
		//print_r($postData);
		 $username = $request->query->get('username'); 
        $password = $request->query->get('password');
		
		$encoder = $this->get('security.password_encoder');
		 $user = new User($username);
         $password = $encoder->encodePassword($user, $password);

        
        $entity = $em->getRepository('AppBundle:User')->findOneBy(array('username' => $username));
	    if (!$entity) {
			return new Response('Incorrect login');
		
		}
		else
		{
			$token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $entity->getUsername(),
                'exp' => time() + 3600 // 1 hour expiration
			]);
			return new Response(sprintf('Logged in as %s <br> Token: %s', $entity->getUsername(), $token));
		}
			
    }

    /**
     * @Route("/login_check", name="security_login_check")
     */
    public function login_check(Request $request)
    {

		$em = $this->getDoctrine()->getManager();
		
		//print_r($postData);
		 $username = $request->query->get('username');
        $password = $request->query->get('password');
        
        $entity = $em->getRepository('AppBundle:User')->findOneBy(array('username' => $username));
	    if (!$entity) {
		return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
		}
		else
			return new Response('Incorrect login');

    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {

    }
}