<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/api/v1/football')) {
            // app_api_v1_football_new
            if ('/api/v1/football' === $pathinfo) {
                $ret = array (  '_controller' => 'AppBundle\\Controller\\Api\\v1\\FootballController::newAction',  '_route' => 'app_api_v1_football_new',);
                if (!in_array($requestMethod, array('POST'))) {
                    $allow = array_merge($allow, array('POST'));
                    goto not_app_api_v1_football_new;
                }

                return $ret;
            }
            not_app_api_v1_football_new:

            // app_api_v1_football_get
            if (preg_match('#^/api/v1/football/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'app_api_v1_football_get')), array (  '_controller' => 'AppBundle\\Controller\\Api\\v1\\FootballController::getAction',));
                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_app_api_v1_football_get;
                }

                return $ret;
            }
            not_app_api_v1_football_get:

            // app_api_v1_football_delete
            if (preg_match('#^/api/v1/football/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'app_api_v1_football_delete')), array (  '_controller' => 'AppBundle\\Controller\\Api\\v1\\FootballController::deleteAction',));
                if (!in_array($requestMethod, array('DELETE'))) {
                    $allow = array_merge($allow, array('DELETE'));
                    goto not_app_api_v1_football_delete;
                }

                return $ret;
            }
            not_app_api_v1_football_delete:

        }

        // api
        if ('/api' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\Api\\v1\\AuthController::api',  '_route' => 'api',);
        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        // register
        if ('/register' === $pathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\Api\\v1\\AuthController::register',  '_route' => 'register',);
            if (!in_array($requestMethod, array('POST'))) {
                $allow = array_merge($allow, array('POST'));
                goto not_register;
            }

            return $ret;
        }
        not_register:

        // login_check
        if ('/login_check' === $pathinfo) {
            $ret = array('_route' => 'login_check');
            if (!in_array($requestMethod, array('POST'))) {
                $allow = array_merge($allow, array('POST'));
                goto not_login_check;
            }

            return $ret;
        }
        not_login_check:

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
